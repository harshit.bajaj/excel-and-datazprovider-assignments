package dataDrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FetchingDataUsingApachePOI {

	public static void main(String[] args) throws Exception {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		
		String path = System.getProperty("user.dir");
		System.out.println(path);
		File file = new File(path + "/TestData2.xlsx");
		FileInputStream fs = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fs);
		XSSFSheet sheet = wb.getSheet("Login");
		int rowCount = sheet.getLastRowNum();
		
		for(int i=0;i<=rowCount;i++) {
			int colCount = sheet.getRow(i).getLastCellNum();// total no. of col which has data
			for(int j=0;j<colCount;j++) {
				String cellValue = sheet.getRow(i).getCell(j).getStringCellValue();
				System.out.print(cellValue+ " ");
			}
			System.out.println();
		}
		
		for(int i=1;i<=rowCount;i++) {
			int colCount = sheet.getRow(i).getLastCellNum();// total no. of col which has data
			for(int j=0;j<colCount-1;j++) {
				String email = sheet.getRow(i).getCell(j).getStringCellValue();
				String password = sheet.getRow(i).getCell(j+1).getStringCellValue();
				driver.get("https://demowebshop.tricentis.com/login");
				driver.findElement(By.id("Email")).sendKeys(email);
				driver.findElement(By.id("Password")).sendKeys(password);
				driver.findElement(By.xpath("//input[@value='Log in']")).click();
				driver.findElement(By.linkText("Log out")).click();
			}
			
		}
		driver.close();
	}

}
