package dataDrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import jxl.Sheet;
import jxl.Workbook;

public class FetchingValueUsingJXL {

	public static void main(String[] args) throws Exception {
		String path = System.getProperty("user.dir");
		System.out.println(path);
		File file = new File(path + "/TestData1.xls");
		FileInputStream fs = new FileInputStream(file);
		Workbook wb = Workbook.getWorkbook(fs);
		Sheet sheet = wb.getSheet("LoginCreds");
		String cellDataUsernameHeading = sheet.getCell(0, 0).getContents(); //.getCell(row,column)
		System.out.println("value present is "+cellDataUsernameHeading);
		String cellDataPasswordValue = sheet.getCell(1, 1).getContents(); //.getCell(row,column)
		System.out.println("value present is "+cellDataPasswordValue);
		
		int rowCount = sheet.getRows(); //total no. of rows which has data
		int columns = sheet.getColumns(); //total no. of columns which has data
		for(int i=0;i<rowCount-1;i++) {
			for(int j=0;j<columns;j++) {
				System.out.print(sheet.getCell(j, i).getContents()+"  ");
			}
			System.out.println();
		}
		
	}

}
