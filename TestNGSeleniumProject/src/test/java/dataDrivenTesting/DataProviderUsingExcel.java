package dataDrivenTesting;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.apache.poi.ss.usermodel.DataFormatter;

public class DataProviderUsingExcel {
	public static DataFormatter formatter= new DataFormatter();
	WebDriver driver = new ChromeDriver();
	
	@AfterClass
	public void quit() {
		driver.close();
	}
	@Test(dataProvider = "dataProviderExcel")
	public void login(String user, String pswd) {
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.findElement(By.id("Email")).sendKeys(user);
		driver.findElement(By.id("Password")).sendKeys(pswd);
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		driver.findElement(By.linkText("Log out")).click();
	}

	@DataProvider
	public Object[][] dataProviderExcel() throws Exception {
		int r=0;
		String path = System.getProperty("user.dir");
		System.out.println(path);
		File file = new File(path + "/TestData2.xlsx");
		FileInputStream fs = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fs);
		XSSFSheet sheet = wb.getSheet("Login");
		int rowCount = sheet.getLastRowNum(); //total no. of rows which has data
		int ColNum=sheet.getRow(0).getLastCellNum(); 
		Object Data[][]= new Object[rowCount][ColNum];

		for(int i=1;i<=rowCount;i++,r++) {
			int colCount = sheet.getRow(i).getLastCellNum();
			for(int j=0,c=0;j<colCount;j++,c++) {
				String value=formatter.formatCellValue(sheet.getRow(i).getCell(j));
				Data[r][c] = value;
			}

		}


		return Data;
	}
}
