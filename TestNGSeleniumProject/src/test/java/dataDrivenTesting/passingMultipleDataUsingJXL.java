package dataDrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;

public class passingMultipleDataUsingJXL {

	public static void main(String[] args) throws Exception {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		String path = System.getProperty("user.dir");
		System.out.println(path);
		File file = new File(path + "/TestData1.xls");
		FileInputStream fs = new FileInputStream(file);
		Workbook wb = Workbook.getWorkbook(fs);
		Sheet sheet = wb.getSheet("LoginCreds");
		String cellDataUsernameHeading = sheet.getCell(0, 0).getContents(); //.getCell(row,column)
		System.out.println("value present is "+cellDataUsernameHeading);
		String cellDataPasswordValue = sheet.getCell(1, 1).getContents(); //.getCell(row,column)
		System.out.println("value present is "+cellDataPasswordValue);
		
		int rowCount = sheet.getRows(); //total no. of rows which has data
		int columns = sheet.getColumns(); //total no. of columns which has data
		
	for(int i=1;i<rowCount;i++) {
		String username = sheet.getCell(0, i).getContents();
		String password = sheet.getCell(1, i).getContents();
		System.out.println(username + "  "+ password);
		driver.get("https://demowebshop.tricentis.com/login");
		driver.findElement(By.id("Email")).sendKeys(username);
		driver.findElement(By.id("Password")).sendKeys(password);
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		driver.findElement(By.linkText("Log out")).click();
	}
	driver.close();
	}

}
