package Assignments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;

public class dataDrivenTestingRegisterApachePOI {

	public static void main(String[] args) throws Exception {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		String path = System.getProperty("user.dir");
		System.out.println(path);
		File file = new File(path + "/TestData2.xlsx");
		FileInputStream fs = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fs);
		XSSFSheet sheet = wb.getSheet("Register");
		int rowCount = sheet.getLastRowNum(); //total no. of rows which has data

		for(int i=1;i<=rowCount;i++) {
			String email = sheet.getRow(i).getCell(0).getStringCellValue();
			String password = sheet.getRow(i).getCell(1).getStringCellValue();
			String firstName = sheet.getRow(i).getCell(2).getStringCellValue();
			String lastName = sheet.getRow(i).getCell(3).getStringCellValue();
			driver.get("https://demowebshop.tricentis.com/register");
			driver.findElement(By.id("FirstName")).sendKeys(firstName);
			driver.findElement(By.id("LastName")).sendKeys(lastName);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);

			driver.findElement(By.id("register-button")).click();
			driver.findElement(By.linkText("Log out")).click();
			driver.findElement(By.linkText("Register")).click();
		}
		driver.close();
	}

}

