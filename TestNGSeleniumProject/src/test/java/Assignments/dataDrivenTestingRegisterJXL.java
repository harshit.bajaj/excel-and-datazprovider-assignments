package Assignments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;

public class dataDrivenTestingRegisterJXL {

	public static void main(String[] args) throws Exception {
			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			String path = System.getProperty("user.dir");
			System.out.println(path);
			File file = new File(path + "/TestData1.xls");
			FileInputStream fs = new FileInputStream(file);
			Workbook wb = Workbook.getWorkbook(fs);
			Sheet sheet = wb.getSheet("RegisterCreds");
			int rowCount = sheet.getRows(); //total no. of rows which has data
			int columns = sheet.getColumns(); //total no. of columns which has data

			for(int i=1;i<rowCount;i++) {
				String email = sheet.getCell(0, i).getContents();
				String password = sheet.getCell(1, i).getContents();
				String firstName = sheet.getCell(2, i).getContents();
				String lastName = sheet.getCell(3, i).getContents();
				driver.get("https://demowebshop.tricentis.com/register");
				driver.findElement(By.id("FirstName")).sendKeys(firstName);
				driver.findElement(By.id("LastName")).sendKeys(lastName);
				driver.findElement(By.id("Email")).sendKeys(email);
				driver.findElement(By.id("Password")).sendKeys(password);
				driver.findElement(By.id("ConfirmPassword")).sendKeys(password);

				driver.findElement(By.id("register-button")).click();
				driver.findElement(By.linkText("Log out")).click();
				driver.findElement(By.linkText("Register")).click();
			}
			driver.close();
		}

}
