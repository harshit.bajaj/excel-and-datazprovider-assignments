package seleniumModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;

public class SeleniumWithTestNG {
	WebDriver driver;
	@Test
	public void buyDesktop() throws Exception {
		WebElement computers = driver.findElement(By.xpath("//ul[@class='top-menu']//child::a[@href='/computers']"));
		Actions act = new Actions(driver);
		act.moveToElement(computers).build().perform();
		driver.findElement(By.partialLinkText("Desktops")).click();
		Select select = new Select(driver.findElement(By.id("products-orderby")));
		select.selectByVisibleText("Price: Low to High");
		List<WebElement> list = driver.findElements(By.xpath("//input[@value='Add to cart']"));
		list.get(0).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='add-to-cart-panel']//input[@value='Add to cart']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='header-links']//a[@class='ico-cart']")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
	}

	@BeforeClass
	public void login() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.findElement(By.id("Email")).sendKeys("harshit18@yopmail.com");
		driver.findElement(By.id("Password")).sendKeys("1234..Hart");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@AfterClass
	public void quit() {
		driver.quit();
	}

	@Test (dependsOnMethods = "buyDesktop")
	public void checkout() throws Exception {
		Select sel = new Select(driver.findElement(By.id("BillingNewAddress_CountryId")));
		sel.selectByValue("1");
		driver.findElement(By.id("BillingNewAddress_City")).sendKeys("ABCD");
		driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("ABCD");
		driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("132003");
		driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("9111111111");
		driver.findElement(By.xpath("//input[@title='Continue']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@id='shipping-buttons-container']//input[@title='Continue']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@id='shipping-method-buttons-container']//input[@value='Continue']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@id='payment-method-buttons-container']//input[@value='Continue']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@id='payment-info-buttons-container']//input[@value='Continue']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@value='Confirm']")).click();
		Thread.sleep(2000);
		System.out.println(driver.findElement(By.xpath("//ul[@class='details']/li")).getText());

	}

}
